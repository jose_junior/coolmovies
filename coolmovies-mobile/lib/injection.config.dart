// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:coolmovies/app/features/home/data/data_sources/home_remote_data_source.dart'
    as _i4;
import 'package:coolmovies/app/features/home/data/data_sources/i_home_data_source.dart'
    as _i3;
import 'package:coolmovies/app/features/home/data/repositories/home_repository.dart'
    as _i6;
import 'package:coolmovies/app/features/home/domain/i_home_repository.dart'
    as _i5;
import 'package:coolmovies/app/features/home/view/home_controller.dart' as _i7;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

/// ignore_for_file: unnecessary_lambdas
/// ignore_for_file: lines_longer_than_80_chars
extension GetItInjectableX on _i1.GetIt {
  /// initializes the registration of main-scope dependencies inside of [GetIt]
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    gh.factory<_i3.IHomeDataSource>(() => _i4.HomeRemoteDataSource());
    gh.factory<_i5.IHomeRepository>(
        () => _i6.HomeRepository(gh<_i3.IHomeDataSource>()));
    gh.factory<_i7.HomeController>(
        () => _i7.HomeController(gh<_i5.IHomeRepository>()));
    return this;
  }
}
