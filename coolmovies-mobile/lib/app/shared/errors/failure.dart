class Failure implements Exception {
  Failure(this.message);
  final String? message;
}

class ServerFailure extends Failure {
  ServerFailure(String? message) : super(message);
}
