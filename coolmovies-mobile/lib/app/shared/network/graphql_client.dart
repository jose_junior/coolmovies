import 'dart:io';

import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class GraphQLConfig {
  static final HttpLink httpLink = HttpLink(
    Platform.isAndroid
        ? 'http://10.0.2.2:5001/graphql'
        : 'http://localhost:5001/graphql',
  );
  static String? _token;
  static final AuthLink authLink = AuthLink(
    getToken: () async => _token,
  );

  final Link link = authLink.concat(httpLink);

  static ValueNotifier<GraphQLClient>? initializeClient(String token) {
    _token = token;
    final client = ValueNotifier(
      GraphQLClient(
        link: authLink,
        cache: GraphQLCache(store: HiveStore()),
      ),
    );

    return client;
  }
}
