import 'package:coolmovies/app/features/home/data/models/movie_model.dart';
import 'package:coolmovies/app/features/home/data/queries/movie_fetch.dart';
import 'package:coolmovies/app/shared/widgets/movie_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    VoidCallback? refetchQuery;
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Cool Movies"),
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 20),
          height: MediaQuery.of(context).size.height,
          child: Query(
            options: QueryOptions(
              document: gql(MovieFetch.fetchAll),
            ),
            builder: (QueryResult result,
                {VoidCallback? refetch, FetchMore? fetchMore}) {
              refetchQuery = refetch;

              if (result.hasException) {
                return const Text("Ops! We couldn't load movies! Try again.");
              }

              if (result.isLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }

              List? movies = List.from(result.data?['allMovies']['nodes'])
                  .map((movie) => MovieModel.fromJson(movie))
                  .toList();

              return GridView.count(
                crossAxisCount: 2,
                mainAxisSpacing: 16,
                childAspectRatio: 0.8,
                children: movies
                    .map(
                      (movie) => AnimationConfiguration.staggeredGrid(
                        position: movies.indexOf(movie),
                        columnCount: 2,
                        duration: const Duration(milliseconds: 700),
                        child: ScaleAnimation(
                          child: FadeInAnimation(
                            child: MovieCard(
                              title: movie.title!,
                              image: movie.imgUrl!,
                            ),
                          ),
                        ),
                      ),
                    )
                    .toList(),
              );
            },
          ),
        ),
      ),
    );
  }
}
