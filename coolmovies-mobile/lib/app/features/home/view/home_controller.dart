import 'package:coolmovies/app/features/home/data/models/movie_model.dart';
import 'package:coolmovies/app/features/home/domain/i_home_repository.dart';
import 'package:get/get.dart';
import 'package:injectable/injectable.dart';

@Injectable()
class HomeController extends GetxController {
  final IHomeRepository _homeRepository;
  final isLoading = Rx<bool>(false);
  final movies = Rx<List<MovieModel>>([]);

  HomeController(this._homeRepository);

  @override
  void onInit() {
    fetchMovies();
    super.onInit();
  }

  Future<void> fetchMovies() async {
    isLoading(true);

    final moviesResponse = await _homeRepository.fetchMovies();

    var result = moviesResponse.fold((l) => l, (r) => r);

    isLoading(false);

    if (result is List<MovieModel>) {
      movies(result);
    } else {
      throw result;
    }
  }
}
