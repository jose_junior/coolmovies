import 'package:coolmovies/app/features/home/data/models/movie_model.dart';

abstract class IHomeDataSource {
  Future<List<MovieModel>> fetchMovies();
}
