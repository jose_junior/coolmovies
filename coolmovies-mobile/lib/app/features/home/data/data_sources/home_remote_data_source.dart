import 'package:coolmovies/app/features/home/data/data_sources/i_home_data_source.dart';
import 'package:coolmovies/app/features/home/data/models/movie_model.dart';
import 'package:coolmovies/app/features/home/data/queries/movie_fetch.dart';
import 'package:coolmovies/app/shared/errors/failure.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: IHomeDataSource)
class HomeRemoteDataSource implements IHomeDataSource {
  //final GraphQLClient client;

  HomeRemoteDataSource();

  @override
  Future<List<MovieModel>> fetchMovies() async {
    try {
      final moviesResult = useQuery(
        QueryOptions(
          document: gql(
            MovieFetch.fetchAll,
          ), // this is the query string you just created
        ),
      );

      final result = moviesResult.result;

      if (result.hasException) {
        throw Failure('Failed loading content');
      }

      return List.from(result.data!['allMovies']['nodes'])
          .map((movie) => MovieModel.fromJson(movie))
          .toList();
    } on Exception catch (_) {
      throw ServerFailure('Failed loading content');
    }
  }
}
