import 'package:json_annotation/json_annotation.dart';

part 'movie_model.g.dart';

@JsonSerializable()
class MovieModel {
  final String? id;
  final String? imgUrl;
  final String? title;
  final String? releaseDate;

  MovieModel({
    this.id,
    this.imgUrl,
    this.title,
    this.releaseDate,
  });

  factory MovieModel.fromJson(Map<String, dynamic>? json) =>
      _$MovieModelFromJson(json!);

  Map<String, dynamic> toJson() => _$MovieModelToJson(this);
}
