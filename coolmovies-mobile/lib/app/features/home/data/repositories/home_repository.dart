import 'package:coolmovies/app/features/home/data/data_sources/i_home_data_source.dart';
import 'package:coolmovies/app/features/home/data/models/movie_model.dart';
import 'package:coolmovies/app/features/home/domain/i_home_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:coolmovies/app/shared/errors/failure.dart';
import 'package:injectable/injectable.dart';

@Injectable(as: IHomeRepository)
class HomeRepository implements IHomeRepository {
  final IHomeDataSource homeDataSource;

  HomeRepository(this.homeDataSource);

  @override
  Future<Either<Failure, List<MovieModel>>> fetchMovies() async {
    try {
      final result = await homeDataSource.fetchMovies();
      return right(result);
    } catch (_) {
      rethrow;
    }
  }
}
