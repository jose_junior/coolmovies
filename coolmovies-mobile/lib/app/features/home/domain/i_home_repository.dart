import 'package:coolmovies/app/features/home/data/models/movie_model.dart';
import 'package:coolmovies/app/shared/errors/failure.dart';
import 'package:dartz/dartz.dart';

abstract class IHomeRepository {
  Future<Either<Failure, List<MovieModel>>> fetchMovies();
}
